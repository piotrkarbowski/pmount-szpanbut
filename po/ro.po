# Romanian translation for mount removable devices as normal user
# Copyright (c) (c) 2005 Canonical Ltd, and Rosetta Contributors 2005
# This file is distributed under the same license as the mount removable devices as normal user package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: mount removable devices as normal user\n"
"Report-Msgid-Bugs-To: martin.pitt@canonical.com\n"
"POT-Creation-Date: 2006-08-15 23:38+0200\n"
"PO-Revision-Date: 2007-12-11 20:30+0000\n"
"Last-Translator: fork <Unknown>\n"
"Language-Team: Romanian <ro@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2010-05-04 20:48+0000\n"
"X-Generator: Launchpad (build Unknown)\n"
"X-Rosetta-Version: 0.1\n"

#: ../src/pmount.c:51
#, c-format
msgid ""
"Usage:\n"
"\n"
"%s [options] <device> [<label>]\n"
"\n"
"  Mount <device> to a directory below %s if policy requirements\n"
"  are met (see pmount(1) for details). If <label> is given, the mount point\n"
"  will be %s/<label>, otherwise it will be %s<device>.\n"
"  If the mount point does not exist, it will be created.\n"
"\n"
msgstr ""
"Folosire:\n"
"%s [optiuni]<dispozitiv>[<label>]\n"
"\n"
"  Mount <dispozitiv> intr-un directorsub %s daca cerintele\n"
"sunt intalnite(vezi pmount(1) pentru detalii). Daca ,label> este dat, "
"punctul de montare va fi %s/<label>, altfel va fi %s<dispozitiv>.\n"
"Daca punctul de montare nu exista, el va fi creat.\n"
"\n"

#: ../src/pmount.c:58
#, c-format
msgid ""
"%s --lock <device> <pid>\n"
"  Prevent further pmounts of <device> until it is unlocked again. <pid>\n"
"  specifies the process id the lock holds for. This allows to lock a device\n"
"  by several independent processes and avoids indefinite locks of crashed\n"
"  processes (nonexistant pids are cleaned before attempting a mount).\n"
"\n"
msgstr ""
"%s --lock <dispozitiv> <pid>\n"
"Previne pmounts-uri viitoare ale <dispozitiv> pana cand este deblocat.\n"
"<pid> specifica id-ul procesului mentinut de lock.Aceasta pemite blocarea "
"unui dispozitiv\n"
"prin cateva procese independente si evita blocari indefinite\n"
"ale proceselor blocate (pid-uri inexistente sunt sterse inainte de "
"montare).\n"
"\n"

#: ../src/pmount.c:65
#, c-format
msgid ""
"%s --unlock <device> <pid>\n"
"  Remove the lock on <device> for process <pid> again.\n"
"\n"
msgstr ""
"%s --unlock <dispozitive> <pid>\n"
"Sterge lock-ul pe <dispozitiv> pentru procesul <pid>.\n"
"\n"

#: ../src/pmount.c:68
msgid ""
"Options:\n"
"  -r          : force <device> to be mounted read-only\n"
"  -w          : force <device> to be mounted read-write\n"
"  -s, --sync  : mount <device> with the 'sync' option (default: 'async')\n"
"  --noatime   : mount <device> with the 'noatime' option (default: 'atime')\n"
"  -e, --exec  : mount <device> with the 'exec' option (default: 'noexec')\n"
"  -t <fs>     : mount as file system type <fs> (default: autodetected)\n"
"  -c <charset>: use given I/O character set (default: 'utf8' if called\n"
"                in an UTF-8 locale, otherwise mount default)\n"
"  -u <umask>  : use specified umask instead of the default (only for\n"
"                file sytems which actually support umask setting)\n"
" --passphrase <file>\n"
"                read passphrase from file instead of the terminal\n"
"                (only for LUKS encrypted devices)\n"
"  -d, --debug : enable debug output (very verbose)\n"
"  -h, --help  : print help message and exit successfuly\n"
"  --version   : print version number and exit successfully"
msgstr ""
"Optiuni:\n"
"  -r : forteaza <dispozitivul> sa fie montat read-only\n"
"  -w : forteaza <dispozitivul> sa fie montat read-write\n"
"  -s, --sync : monteaza <dispozitivul> cu optiunea 'sync' (standard: "
"'async')\n"
"  --noatime : monteaza <dispozitivul> cu optiunea 'noatime' (standard: "
"'atime')\n"
"  -e, --exec : monteaza <dispozitivul> cu optiunea 'exec' (standard: "
"'noexec')\n"
"  -t <fs> : monteaza ca tip de sistem de fisiere <fs> (standard: "
"autodetected)\n"
"  -c <charset>: foloseste setul de caractere I/O dat (default: 'utf8' daca e "
"cheamat\n"
"                UTF-8 locale, altfel se monteaza standard)\n"
"  -u <umask> : foloseste umask specificat in locul celui standard (doar "
"pentru\n"
"                sisteme de fisiere care accepta setari umask)\n"
" --passphrase <file>\n"
"                citeste passphrase-ul din fisier in loc de terminal\n"
"                (doar pentru dispozitive encriptate LUKS)\n"
"  -d, --debug : porneste afisarea debug (foarte verbose)\n"
"  -h, --help : arata mesajul de ajutor si inchide\n"
"  --version : arata numarul versiunii si inchide"

#: ../src/pmount.c:134
#, c-format
msgid "Error: label must not be empty\n"
msgstr "Eroare: <label> nu poate fi gol\n"

#: ../src/pmount.c:138
#, c-format
msgid "Error: label too long\n"
msgstr "Eroare: <label> prea lung\n"

#: ../src/pmount.c:143
#, c-format
msgid "Error: '/' must not occur in label name\n"
msgstr "Eroare:'/' nu trebuie sa apara in numele <label>\n"

#: ../src/pmount.c:150
#, c-format
msgid "Error: device name too long\n"
msgstr "Eroare: nume dispozitiv prea lung\n"

#: ../src/pmount.c:181 ../src/pumount.c:100
msgid "Error: could not drop all uid privileges"
msgstr "Eroare: nu pot scadea toate privilegiile uid"

#: ../src/pmount.c:186
msgid "Error: could not execute mount"
msgstr "Eroare: nu pot executa mount"

#: ../src/pmount.c:225
#, c-format
msgid "Internal error: mount_attempt: given file system name is NULL\n"
msgstr ""
"Eroare interna: mount_attempt: numele sistemului de fisiere este NULL\n"

#: ../src/pmount.c:231
#, c-format
msgid "Error: invalid file system name '%s'\n"
msgstr "Eroare: numele '%s' sistemului de fisiere este invalid\n"

#: ../src/pmount.c:237
#, c-format
msgid "Error: invalid umask %s\n"
msgstr "Eroare: umask invalid %s\n"

#: ../src/pmount.c:285
#, c-format
msgid "Error: invalid charset name '%s'\n"
msgstr "Eroare: numele '%s' setului de caractere este invalid\n"

#: ../src/pmount.c:366
#, c-format
msgid "Error: cannot lock for pid %u, this process does not exist\n"
msgstr "Eroare: nu pot bloca pid-ul %u, acest proces nu exista\n"

#: ../src/pmount.c:380
#, c-format
msgid "Error: could not create pid lock file %s: %s\n"
msgstr "Eroare: nu pot crea fisierul pid lock %s: %s\n"

#: ../src/pmount.c:420
#, c-format
msgid "Error: could not remove pid lock file %s: %s\n"
msgstr "Eroare: nu pot sterge fisierul pid lock %s:%s\n"

#: ../src/pmount.c:436
msgid "Error: do_unlock: could not remove lock directory"
msgstr "Eroare: do unlocl: nu mot sterge directorul lock"

#: ../src/pmount.c:537 ../src/pumount.c:165
msgid "Error: this program needs to be installed suid root\n"
msgstr "Eroare: programul trebuie instalat cu suid root\n"

#: ../src/pmount.c:580 ../src/pumount.c:187
#, c-format
msgid "Internal error: getopt_long() returned unknown value\n"
msgstr "Eroare interna: getopt_logn() a intor valoare necunoscuta\n"

#: ../src/pmount.c:618
#, c-format
msgid ""
"Warning: device %s is already handled by /etc/fstab, supplied label is "
"ignored\n"
msgstr ""
"Atentie: dispozitivul %s este folosti de /etc/fstab, eticheta furnizata este "
"ignorata\n"

#: ../src/pmount.c:632 ../src/pumount.c:232
msgid "Error: could not determine real path of the device"
msgstr "Eroare: nu pot determina calea reala a dispozitivului"

#: ../src/pmount.c:641 ../src/pumount.c:238
#, c-format
msgid "Error: invalid device %s (must be in /dev/)\n"
msgstr "Eroare: dispozitiv %s invalid (necesar sa fie in /dev/)\n"

#: ../src/pmount.c:679
#, c-format
msgid "Error: could not decrypt device (wrong passphrase?)\n"
msgstr "Eroare: nu am putut decripta dispozitivul (passphrase gresita?)\n"

#: ../src/pmount.c:682
#, c-format
msgid "Error: mapped device already exists\n"
msgstr "Eroare: Exista deja un despozitiv cartat (mapped)\n"

#: ../src/pmount.c:696
#, c-format
msgid ""
"Error: could not lock the mount directory. Another pmount is probably "
"running for this mount point.\n"
msgstr ""
"Eroare: nu s-a putut bloca directorul. Probabil exista deja lansat un alt "
"pmount pentru acest punct de montare.\n"

#: ../src/pmount.c:720
msgid "Error: could not delete mount point"
msgstr "Eroare: nu pot sterge punctul de montare"

#: ../src/pmount.c:741
#, c-format
msgid "Internal error: mode %i not handled.\n"
msgstr "Eroare interna: mod %i nesuportat.\n"

#: ../src/pmount-hal.c:31
msgid ""
"pmount-hal - execute pmount with additional information from hal\n"
"\n"
"Usage: pmount-hal <device> [pmount options]\n"
"\n"
"This command mounts the device described by the given device or UDI using\n"
"pmount. The file system type, the volume storage policy and the desired "
"label\n"
"will be read out from hal and passed to pmount."
msgstr ""
"pmount-hal - executa pmount cu informatii suplimentare de la hal\n"
"\n"
"Folosire: pmount-hal <dispozitiv> [optiuni pmount]\n"
"\n"
"Aceasta comanda monteaza dispozitivul in functie de dispozitivul dat sau de "
"UDI\n"
"folosind pmount. Sistemul de fisiere, politica de depozitare a volumului si "
"eticheta\n"
"dorita vor fi citite fin hal si pasate lui pmount."

#: ../src/pmount-hal.c:167
msgid "Error: could not execute pmount\n"
msgstr "Eroare: nu pot executa pmount\n"

#: ../src/pmount-hal.c:210
#, c-format
msgid "Error: could not connect to dbus: %s: %s\n"
msgstr "Eroare: nu mă pot conecta la dbus: %s: %s\n"

#: ../src/pmount-hal.c:244
#, c-format
msgid "Error: given UDI is not a mountable volume\n"
msgstr "Eroare: UDI-ul dat nu e un volum montabil\n"

#: ../src/policy.c:92
msgid "Error: could not get status of device"
msgstr "Eroare: nu pot afla starea dispozitivului"

#: ../src/policy.c:103
msgid "Error: could not get sysfs directory\n"
msgstr "Eroare: nu pot obţine directorul sysfs\n"

#: ../src/policy.c:110
msgid "Error: could not open <sysfs dir>/block/"
msgstr "Eroare: nu pot deschide <sysfs dir>/block/"

#: ../src/policy.c:141
msgid "Error: could not open <sysfs dir>/block/<device>/"
msgstr "Eroare: nu pot deschide <sysfs dir>/block/<device>/"

#: ../src/policy.c:241
#, c-format
msgid "Error: device %s does not exist\n"
msgstr "Eroare: dispozitivul %s nu există\n"

#: ../src/policy.c:246
#, c-format
msgid "Error: %s is not a block device\n"
msgstr "Eroare: %s nu e un dispozitiv de blocuri\n"

#: ../src/policy.c:264 ../src/policy.c:320
msgid "Error: could not open fstab-type file"
msgstr "Eroare: nu am putut deschide fisierul fstab-type"

#: ../src/policy.c:351
#, c-format
msgid "Error: device %s is already mounted to %s\n"
msgstr "Eroare: dispozitivul %s este deja montat în %s\n"

#: ../src/policy.c:353
#, c-format
msgid "Error: device %s is not mounted\n"
msgstr "Eroare: dispozitivul %s nu este montat\n"

#: ../src/policy.c:355
#, c-format
msgid "Error: device %s was not mounted by you\n"
msgstr "Eroare: dispozitivul %s nu a fost montat de catre tine\n"

#: ../src/policy.c:391
#, c-format
msgid "Error: device %s is not removable\n"
msgstr "Eroare: dispozitivul %s nu este amovibil\n"

#: ../src/policy.c:454
#, c-format
msgid "Error: device %s is locked\n"
msgstr "Eroare: dispozitivul %s este blocat\n"

#: ../src/policy.c:463
#, c-format
msgid "Error: mount point %s is already in /etc/fstab\n"
msgstr "Eroare: punctul de mount %s exista deja in /etc/fstab\n"

#: ../src/policy.c:476
#, c-format
msgid "Error: directory %s already contains a mounted file system\n"
msgstr "Eroare: directorul %s contine deja un file system montat\n"

#: ../src/policy.c:478
#, c-format
msgid "Error: directory %s does not contain a mounted file system\n"
msgstr "Eroare: directorul %s nu conţine un sistem de fişiere montat\n"

#: ../src/pumount.c:43
#, c-format
msgid ""
"Usage:\n"
"\n"
"%s [options] <device>\n"
"  Umount <device> from a directory below %s if policy requirements\n"
"  are met (see pumount(1) for details). The mount point directory is "
"removed\n"
"  afterwards.\n"
"\n"
"Options:\n"
"  -l, --lazy  : umount lazily, see umount(8)\n"
"  -d, --debug : enable debug output (very verbose)\n"
"  -h, --help  : print help message and exit successfuly\n"
"  --version   : print version number and exit successfully\n"
msgstr ""
"Folosire:\n"
"\n"
"%s [optiuni] <dispozitiv>\n"
"  Demonteaza <dispozitiv> dintr-un director sub %s daca cererile sunt\n"
"  satisfacute (vezi pumount(1) pentru detalii). Directorul punct de\n"
"  montare este apoi sters.\n"
"\n"
"Optiuni:\n"
"-l, --lazy : demonteaza lent, vezi umount(8)\n"
"-d, --debug : porneste afisarea debug (foarte verbose)\n"
"-h, --help : arata mesajul de ajutor apoi iese\n"
"--version : arata numarul versiunii apoi iese\n"

#: ../src/pumount.c:74
msgid "Internal error: could not determine mount point\n"
msgstr "Eroare internă: nu pot determina punctul de montare\n"

#: ../src/pumount.c:80
#, c-format
msgid "Error: mount point %s is not below %s\n"
msgstr "Eroare: punctul de montare %s nu se află sub %s\n"

#: ../src/pumount.c:110
msgid "Error: could not execute umount"
msgstr "Eroare: nu pot executa umount"

#: ../src/pumount.c:131
#, c-format
msgid "Error: umount failed\n"
msgstr "Eroare: umount a eşuat\n"

#: ../src/utils.c:52
#, c-format
msgid "Error: out of memory\n"
msgstr "Eroare: memorie insuficientă\n"

#: ../src/utils.c:108
msgid "Error: could not create directory"
msgstr "Eroare: nu pot crea directorul"

#: ../src/utils.c:124
msgid "Error: could not create stamp file in directory"
msgstr "Eroare: nu am putut crea fisierul stamp in director"

#: ../src/utils.c:132
#, c-format
msgid "Error: %s is not a directory\n"
msgstr "Eroare: %s nu este un director\n"

#: ../src/utils.c:152
msgid "Error: could not open directory"
msgstr "Eroare: nu pot deschide directorul"

#: ../src/utils.c:161
#, c-format
msgid "Error: directory %s is not empty\n"
msgstr "Eroare: directorul %s nu este gol\n"

#: ../src/utils.c:213
#, c-format
msgid "Error: '%s' is not a valid number\n"
msgstr "Eroare: '%s' nu este un număr valid\n"

#: ../src/utils.c:255
msgid "Internal error: could not change to effective uid root"
msgstr "Eroare interna: nu am putut schimba in uid-ul root efectiv"

#: ../src/utils.c:264
msgid "Internal error: could not change effective user uid to real user id"
msgstr "Eroare interna: nu pot schimba uid efectiv in user id-ul real"

#: ../src/utils.c:273
msgid "Internal error: could not change to effective gid root"
msgstr "Eroare interna: nu pot schimba in root gid efectiv"

#: ../src/utils.c:282
msgid "Internal error: could not change effective group id to real group id"
msgstr "Eroare interna: nu pot schimba group id efectiv in group id real"
