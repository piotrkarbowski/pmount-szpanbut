/* Just a stub, for cmake build system. To be dropped later */

/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* defined if pmount-hal frontend should be built */
#undef BUILD_HAL

/* path to cryptsetup */
#define CRYPTSETUP "/sbin/cryptsetup"

/* Define this parameter to run cryptsetup with RUID = EUID = root */
#define CRYPTSETUP_RUID 1

/* always defined to indicate that i18n is enabled */
#define ENABLE_NLS 1

/* path to fsck */
#define FSCKPROG "/sbin/fsck"

/* Gettext package */
#define GETTEXT_PACKAGE "pmount"

/* Define to 1 if you have the `bind_textdomain_codeset' function. */
#define HAVE_BIND_TEXTDOMAIN_CODESET 1

/* Description */
#define HAVE_BLKID 1

/* Define to 1 if you have the `dcgettext' function. */
#define HAVE_DCGETTEXT 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define if the GNU gettext() function is already present or preinstalled. */
#define HAVE_GETTEXT 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define if your <locale.h> file defines LC_MESSAGES. */
#define HAVE_LC_MESSAGES 1

/* Define to 1 if you have the <locale.h> header file. */
#define HAVE_LOCALE_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* directory for locks */
#define LOCKDIR "/var/lib/pmount-locks"

/* path to losetup */
#define LOSETUPPROG "/sbin/losetup"

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#define LT_OBJDIR ".libs/"

/* directory for mounts */
#define MEDIADIR "/media/"

/* path to mount */
#define MOUNTPROG "/bin/mount"

/* location of mount.ntfs-3g */
#define MOUNT_NTFS_3G "/sbin/mount.ntfs-3g"

/* Name of package */
#define PACKAGE "pmount"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME ""

/* Define to the full name and version of this package. */
#define PACKAGE_STRING ""

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME ""

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION ""

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* system configuration file */
#define SYSTEM_CONFFILE "/etc/pmount.conf"

/* path to umount */
#define UMOUNTPROG "/bin/umount"

/* Version number of package */
#define VERSION "0.9.99-alpha"

/* whitelist configuration file */
#define WHITELIST "/etc/pmount.allow"
