add_executable(pmount
        conffile.c
        configuration.c
        fs.c
        loop.c
        luks.c
        pmount.c
        policy.c
        realpath.c
        utils.c
)

add_executable(pumount
        policy.c
        realpath.c
        luks.c
        utils.c
        conffile.c
        configuration.c
        pumount.c
)

include_directories(.)

pkg_check_modules(UUID uuid REQUIRED)
pkg_check_modules(BLKID blkid REQUIRED)

link_directories(
        ${UUID_LIBRARY_DIRS}
        ${BLKID_LIBRARY_DIRS}
)

include_directories(
        ${UUID_INCLUDE_DIRS}
        ${BLKID_INCLUDE_DIRS}
)

target_link_libraries(pmount
        ${UUID_LIBRARIES}
        ${BLKID_LIBRARIES}
)
